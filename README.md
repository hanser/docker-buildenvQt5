Dockerfiles and Drone-config for creating Docker images for qt5 development

# ARM build

Can't get it to work with drone docker plugin

Resigning and building by hand on odroid with armbian

	git clone https://codeberg.org/hanser/docker-buildenvQt5.git

# GLibC
	cd dockerBuildenvQt5/glibc

	docker build --no-cache -t buildenv-glibc-qt5:latest-linux-arm -f Dockerfile-qt5 .
	docker tag buildenv-glibc-qt5:latest-linux-arm hansers/buildenv-glibc-qt5:latest-linux-arm
	docker push hansers/buildenv-glibc-qt5:latest-linux-arm

	docker build --no-cache -t buildenv-glibc-qt5-opencv:latest-linux-arm -f Dockerfile-qt5-opencv .
	docker tag buildenv-glibc-qt5-opencv:latest-linux-arm hansers/buildenv-glibc-qt5-opencv:latest-linux-arm
	docker push hansers/buildenv-glibc-qt5-opencv:latest-linux-arm

# MusLibC
	cd dockerBuildenvQt5/muslibc

	docker build --no-cache -t buildenv-muslibc-qt5:latest-linux-arm -f Dockerfile-qt5 .
	docker tag buildenv-muslibc-qt5:latest-linux-arm hansers/buildenv-muslibc-qt5:latest-linux-arm
	docker push hansers/buildenv-muslibc-qt5:latest-linux-arm

	docker build --no-cache -t buildenv-muslibc-qt5-opencv:latest-linux-arm -f Dockerfile-qt5-opencv .
	docker tag buildenv-muslibc-qt5-opencv:latest-linux-arm hansers/buildenv-muslibc-qt5-opencv:latest-linux-arm
	docker push hansers/buildenv-muslibc-qt5-opencv:latest-linux-arm


